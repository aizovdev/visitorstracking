<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('db', 'TestController@dbconnection');
$router->get('cacheclear', 'TestController@cacheclear');
$router->get('config', 'TestController@config');
// API route group
$router->group(['prefix' => 'api'], function () use ($router) {
    // Matches "/api/register
    $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@login');
    $router->post('visitors_logs', ['uses' => 'VisitorsLogsController@create']);
    $router->get('device_count', ['uses' => 'VisitorsLogsController@getDeviceCount']);
    $router->get('device_count_all', ['uses' => 'VisitorsLogsController@getDeviceCountAll']);
    $router->get('visitors_country_all', ['uses' => 'VisitorsLogsController@getVisitorsCountryCountAll']);
 });
<?php

namespace App\Http\Controllers;

use App\VisitorsLog;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VisitorsLogsController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @author Dino
     * @date 19-02-2020
     * store visitors detail
     * @return Response
     */
    public function create(Request $request)
    {
        // echo '<pre>'; print_r($request->all());exit;
        // $ip = $request->ip();
        $address = "";
        $ip_apiresult = $this->validationIP_API("http://ip-api.com/json/$request->ip_address?fields=country,lat,lon,timezone");
        $latitude = $ip_apiresult['lat'];
        $longitude = $ip_apiresult['lon'];
        $location_result = $this->validationIP_API("https://maps.googleapis.com/maps/api/geocode/json?latlng=$latitude,$longitude&key=AIzaSyCbfMjco6LRNimffgKp7E06Qax7MQ_VONg");
        $timestamp = time();
        if ($location_result != "false") {
            $address = $location_result['results'][0]["formatted_address"];
        }
        $query = VisitorsLog::where('ip_address', $request->ip_address)
            ->where('start_time', '>=', Carbon::now()->subMinutes(1)->toDateTimeString())
            ->first();
        if (!$query) {
            $details = VisitorsLog::create([
                'ip_address' => $request->ip_address,
                'lat' => $latitude,
                'lon' => $longitude,
                'address' => $address,
                'country' => $ip_apiresult['country'],
                'time_zone' => $ip_apiresult['timezone'],
                'start_time' => Carbon::now(),
                // 'end_time' => "",
                'device' => $request->device,
            ]);
            return response()->json(true, 201);
        } else {
            return response()->json(false, 201);
        }
    }
    /**
     * api validation
     * @return Response
     */
    public function validationIP_API($request)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $request);
        $status_code = $response->getStatusCode();
        $value = $response->getBody();
        $data = json_decode($value, true);
        if ($status_code != 200) {
            $data = "false";
        }
        return $data;
    }

    /**
     * @author Dino
     * @date 05-12-2019
     *
     * @return Response
     */
    public function getDeviceCount()
    {
        $mobile = VisitorsLog::where('device', '=', 'Mobile')
            ->where('start_time', '>=', Carbon::now()->subDays(30))
            ->count();
        $tablet = VisitorsLog::where('device', '=', 'Tablet')
            ->where('start_time', '>=', Carbon::now()->subDays(30))
            ->count();
        $desktop = VisitorsLog::where('device', '=', 'Desktop')
            ->where('start_time', '>=', Carbon::now()->subDays(30))
            ->count();
        return ['Mobile' => $mobile, 'Tablet' => $tablet, 'Desktop' => $desktop];
    }
    /**
     * @author Dino
     * @date 05-12-2019
     *
     * @return Response
     */
    public function getDeviceCountAll()
    {
        $mobile = VisitorsLog::where('device', '=', 'Mobile')
            ->count();
        $tablet = VisitorsLog::where('device', '=', 'Tablet')
            ->count();
        $desktop = VisitorsLog::where('device', '=', 'Desktop')
            ->count();
        return ['Mobile' => $mobile, 'Tablet' => $tablet, 'Desktop' => $desktop];
    }
    /**
     * @author Dino
     * @date 05-12-2019
     *
     * @return Response
     */
    public function getVisitorsCountryCountAll()
    {
        $data = VisitorsLog::selectRaw('year(created_at) year, country, count(*) data')
            ->whereDate('created_at', '>', Carbon::now()->subYears(2))
            ->groupBy('year', 'country')
            ->orderBy('year', 'asc')
            ->get();
        $countries = VisitorsLog::groupBy('country')
            ->pluck('country');

        return ['data' => $data, 'countries' => $countries];
    }
}

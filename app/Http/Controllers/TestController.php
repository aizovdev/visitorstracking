<?php

namespace App\Http\Controllers;

use Cache;
use Exception;
use DB;
use App\User;
use Laravel\Lumen\Routing\Controller as BaseController;

class TestController extends BaseController
{

    public function cacheclear()
    {
        Cache::flush();
        return 'success';
    }

    public function dbconnection()
    {
        try {
            $data = User::first();
            echo 'hi';
            return $data;

            // if (DB::connection()->getDatabaseName()) {
            //     return 'Connected to the DB: ' . DB::connection()->getDatabaseName();
            // }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    public function config()
    {
        try {
            return config()->all();

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}

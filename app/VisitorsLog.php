<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorsLog extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ip_address',
        'lat',
        'lon', 
        'address', 
        'country', 
        'time_zone',
        'start_time',
        'end_time',
        'device'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}